The first user to signup is made the Baserow admin.

New user signup is enabled by default and the admin can disable this behavior from within Baserow.
