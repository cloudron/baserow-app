#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const DISPLAY_NAME = 'test cloudron';
    const EMAIL = 'test@cloudron.local';
    const PASSWORD = 'Changeme?123';
    const DATABASE_NAME = 'some fancy database';

    let browser, app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function createAccount() {
        await browser.get(`https://${app.fqdn}/signup`);
        await waitForElement(By.xpath('//input[contains(@placeholder, "email")]'));
        await browser.findElement(By.xpath('//input[contains(@placeholder, "email")]')).sendKeys(EMAIL);
        await browser.findElement(By.xpath('//input[contains(@placeholder, "name")]')).sendKeys(DISPLAY_NAME);
        await browser.findElement(By.xpath('//input[contains(@placeholder, "Enter your password")]')).sendKeys(PASSWORD);
        await browser.findElement(By.xpath('//button')).click();
        await waitForElement(By.xpath('//a[contains(., "I don\'t want help setting up")]'));
        await browser.findElement(By.xpath('//a[contains(., "I don\'t want help setting up")]')).click();
    }

    async function login() {
        await browser.get(`https://${app.fqdn}`);
        await waitForElement(By.xpath('//input[contains(@placeholder, "email")]'));
        await browser.findElement(By.xpath('//input[contains(@placeholder, "email")]')).sendKeys(EMAIL);
        await browser.findElement(By.xpath('//input[contains(@placeholder, "Enter your password")]')).sendKeys(PASSWORD);
        await browser.findElement(By.xpath('//button')).click();
        await waitForElement(By.xpath('//span[contains(text(), "Add new...")]'));
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}`);
        await waitForElement(By.xpath('//span[@class="sidebar__workspaces-selector-selected-workspace"]'));
        await browser.findElement(By.xpath('//span[@class="sidebar__workspaces-selector-selected-workspace"]')).click();
        await waitForElement(By.xpath('//a[contains(text(), "Log out")]'));
        await browser.findElement(By.xpath('//a[contains(text(), "Log out")]')).click();
        await waitForElement(By.xpath('//input[contains(@placeholder, "Enter your password")]'));
    }

    async function createDatabase() {
        await browser.get(`https://${app.fqdn}`);

        await waitForElement(By.xpath('//span[contains(text(), "Add new...")]'));
        await browser.findElement(By.xpath('//span[contains(text(), "Add new...")]')).click();

        await waitForElement(By.xpath('(//ul//li//span[contains(text(), "Database")])[1]'));
        await browser.findElement(By.xpath('(//ul//li//span[contains(text(), "Database")])[1]')).click();

        await waitForElement(By.xpath('//div[@class="modal__box"]//input[@type="text"]'));
        await browser.findElement(By.xpath('//div[@class="modal__box"]//input[@type="text"]')).sendKeys(DATABASE_NAME);
        await browser.findElement(By.xpath('//div[@class="modal__box"]//span[contains(text(), "Add")]')).click();

        await waitForElement(By.xpath(`//span[contains(., "${DATABASE_NAME}")]`));
    }

    async function databaseExists() {
        await browser.get(`https://${app.fqdn}`);
        await waitForElement(By.xpath(`//span[contains(text(), "${DATABASE_NAME}")]`));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can create account', createAccount);
    // it('can login', login);
    it('can create database', createDatabase);
    it('workspace exists', databaseExists);
    it('can logout', logout);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS); });

    it('can login', login);
    it('workspace exists', databaseExists);
    it('can logout', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can login', login);
    it('workspace exists', databaseExists);
    it('can logout', logout);

    it('move to different location', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);
    it('can login', login);
    it('workspace exists', databaseExists);
    it('can logout', logout);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('can install app from appstore for update', function () { execSync(`cloudron install --appstore-id io.baserow.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can create account', createAccount);
    it('can create database', createDatabase);
    it('can logout', logout);

    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });

    it('can login', login);
    it('workspace exists', databaseExists);
    it('can logout', logout);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});
