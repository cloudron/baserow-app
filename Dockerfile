FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8
ENV TMPDIR=/run/temp

# https://gitlab.com/baserow/baserow/-/blob/master/web-frontend/package.json#L9 engines.node
ARG NODE_VERSION=21.7.3
RUN mkdir -p /usr/local/node-$NODE_VERSION && \
    curl -L https://nodejs.org/dist/v$NODE_VERSION/node-v$NODE_VERSION-linux-x64.tar.gz | tar zxf - --strip-components 1 -C /usr/local/node-$NODE_VERSION
ENV PATH /usr/local/node-$NODE_VERSION/bin:$PATH

# Suggested at https://forum.cloudron.io/topic/9316/missing-library-for-sso
RUN apt-get -y update && apt -y install xmlsec1 && rm -rf /etc/ssh_host_* && rm -rf /var/cache/apt /var/lib/apt/lists

# renovate: datasource=gitlab-releases depName=baserow/baserow versioning=semver registryUrl=https://gitlab.com
ARG BASEROW_VERSION=1.31.1

RUN curl -LSs "https://gitlab.com/bramw/baserow/-/archive/${BASEROW_VERSION}/baserow-${BASEROW_VERSION}.tar.gz" | tar -xz -C /app/code/ --strip-components 1 -f -
RUN virtualenv -p python3 env
RUN /app/code/env/bin/pip install --no-cache -r /app/code/backend/requirements/base.txt && \
    cd web-frontend && yarn install && yarn build && cd .. && \
    chown -R cloudron:cloudron /app/code

ENV PYTHONPATH $PYTHONPATH:/app/code/backend/src:/app/code/premium/backend/src:/app/code/enterprise/backend/src

# add supervisor configs
ADD supervisor/* /etc/supervisor/conf.d/
RUN sed -e 's,^logfile=.*$,logfile=/run/supervisord.log,' -i /etc/supervisor/supervisord.conf

COPY start.sh nginx.conf /app/pkg/

CMD ["/app/pkg/start.sh"]
