# there are many files, I think we use the first one
# https://github.com/bram2w/baserow/blob/master/docs/installation/configuration-files/nginx.conf#L80
# https://github.com/bram2w/baserow/blob/master/docs/installation/install-behind-nginx.md#L69
# https://github.com/bram2w/baserow/blob/master/deploy/nginx/no-caddy/nginx.conf#L13

user cloudron;

worker_processes 1;
pid /run/nginx.pid;

error_log /dev/stderr warn;

events {
    worker_connections 768;
}

http {
    client_body_temp_path /run/client_body;
    proxy_temp_path /run/proxy_temp;
    fastcgi_temp_path /run/fastcgi_temp;
    scgi_temp_path /run/scgi_temp;
    uwsgi_temp_path /run/uwsgi_temp;

    error_log /dev/stderr warn;
    log_format simple '$remote_addr [$time_local] "$request" $status $body_bytes_sent "$http_referer"';
    access_log /dev/stdout simple;

    server {
        access_log /dev/stdout;
        sendfile on;
        tcp_nopush on;
        tcp_nodelay on;
        keepalive_timeout 65;
        types_hash_max_size 2048;
        include /etc/nginx/mime.types;
        default_type application/octet-stream;

        gzip on;
        gzip_disable "msie6";
        listen 80;
        proxy_read_timeout 1800s;
        client_max_body_size 0; # avoid HTTP 413 for large image uploads
        # required to avoid HTTP 411: see Issue #1486 (https://github.com/dotcloud/docker/issues/1486)
        chunked_transfer_encoding on;

        # this is the web frontend
        location / {
            proxy_pass http://127.0.0.1:3000;
            proxy_pass_request_headers on;
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection "upgrade";
            proxy_read_timeout 86400;
        }

        location ~ ^/(ws)/ {
            proxy_pass http://127.0.0.1:8000;
            proxy_pass_request_headers on;
            proxy_http_version 1.1;
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection "upgrade";
            proxy_read_timeout 86400;
        }

        location ~ ^/(api)/ {
            proxy_pass http://127.0.0.1:8001;
            proxy_pass_request_headers on;
            proxy_http_version 1.1;
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection "upgrade";
            proxy_read_timeout 86400;
        }

        # media delivery
        location /media/ {
            root /app/data; # not a typo! only /app/data/media is served. https://nginx.org/en/docs/http/ngx_http_core_module.html#root 
            add_header Content-Disposition "attachment";
        }
    }
}

