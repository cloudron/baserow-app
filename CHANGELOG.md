[0.1.0]
* Initial version with Baserow 1.6.0

[0.2.0]
* Update to Baserow version 1.7.1

[1.0.0]
* First stable version based on Baserow 1.7.1

[1.1.0]
* Update Baserow to 1.8.2
* [Release announcement](https://baserow.io/blog/january-2022-release-of-baserow)
* Public View Sharing
* Gallery View
* Internationalization
* Rating Field
* User file previews

[1.2.0]
* Update Baserow to 1.9.1
* [Release announcement](https://baserow.io/blog/march-2022-release-of-baserow)
* Footer Aggregations
* Multiple cell copying

[1.3.0]
* Update Baserow to 1.10.0
* [Release announcement](https://baserow.io/blog/may-2022-release-of-baserow)
* Multiple cell paste
* Undo redo functionality
* Row coloring
* Zapier integration

[1.3.1]
* Update Baserow to 1.10.1

[1.3.2]
* Update Baserow to 1.10.2

[1.4.0]
* Update Baserow to 1.11.0
* [Release announcement](https://baserow.io/blog/1-11-release-of-baserow)
* Additional importing
* Snapshots
* Public gallery view sharing
* New Templates

[1.5.0]
* Update Baserow to 1.12.0
* [Release announcement](https://baserow.io/blog/1-12-release-of-baserow)
* Form survey mode
* Multiple collaborators field type
* Duplicate fields with data
* Create a new row directly from the row select modal
* Add files to the form view

[1.5.1]
* Update Baserow to 1.12.1
* [Release announcement](https://github.com/bram2w/baserow/releases/tag/1.12.1)
* adds public sharing to Kanban views
* link and button formula functions 
* better import previews
* env vars for controlling web-hook security
* few more bug fixes

[1.6.0]
* Update Baserow to 1.13.0
* [Release announcement](https://baserow.io/blog/1-13-release-of-baserow)

[1.6.1]
* Update Baserow to 1.13.1

[1.6.2]
* Update Baserow to 1.13.2
* [Full changelog](https://gitlab.com/bramw/baserow/-/releases/1.13.2)
* Add drag and drop zone for files to the row edit modal #1161
* Automatically enable/disable enterprise features upon activation/deactivation without needing a page refresh first. #1306
* Allow creating a new option by pressing enter in the dropdown #1169
* Improved grid view on smaller screens by not making the primary field sticky. #690
* New items automatically get a new name in the modal. 1166
* Don't require password verification when deleting user account. #1401
* Fixed the Heroku deployment template. #1420
* Fixed bug where only one condition per field was working in form's views. #1400
* Fix "ERR_REDIRECT" for authenticated users redirected to the dashboard from the signup page. 1125
* Fixed a problem of some specific error messages not being recognized by the web front-end.
* Fixed failing webhook call log creation when a table has more than one webhooks. #1100

[1.6.3]
* Update Baserow to 1.13.3
* [Full changelog](https://gitlab.com/bramw/baserow/-/releases/1.13.3)

[1.7.0]
* Update Baserow to 1.14.0
* [Full changelog](https://gitlab.com/bramw/baserow/-/releases/1.14.0)

[1.7.1]
* Fix permission issue in data directory

[1.7.2]
* Update Baserow to 1.15.0
* [Full changelog](https://gitlab.com/bramw/baserow/-/releases/1.15.0)

[1.7.3]
* Update Baserow to 1.15.1
* [Full changelog](https://gitlab.com/bramw/baserow/-/releases/1.15.1)

[1.7.4]
* Update Baserow to 1.15.2
* [Full changelog](https://gitlab.com/bramw/baserow/-/releases/1.15.2)
* fixes a bug with prefilled form date fields not working with the correct date format.

[1.8.0]
* Update Baserow to 1.16.0
* [Full changelog](https://gitlab.com/bramw/baserow/-/releases/1.16.0)
* Introduces calendar view

[1.9.0]
* Update Baserow to 1.17.1
* Update nodejs to 18.15.0
* Make gunicorn listen on ipv4 and ipv6
* [Full changelog](https://gitlab.com/bramw/baserow/-/releases/1.17.0)

[1.9.1]
* Update Baserow to 1.17.2
* [Full changelog](https://gitlab.com/bramw/baserow/-/releases/1.17.2)
* Bug fixes, new filters and Google/Azure storage backends

[1.10.0]
* Add xmlsec1 to make OIDC work
* Rename `/app/data/env` to `/app/data/env.sh` to match configuration style

[1.11.0]
* Update Baserow to 1.18.0
* [Full changelog](https://gitlab.com/bramw/baserow/-/releases/1.18.0)
* When copy-pasting, automatically create missing number of rows #1252
* Add 2 date filters: before or same date and after or same date. #1344
* Wrap migrate command in some sort of lock to prevent buggy concurrent migration runs in deployments with many Baserow backend services. #1654
* add ability to copy and paste between different multi select option fields based on matching #1750
* Introduced rollup field #222
* Introduced count field #224
* Fix redirect param being propagated #1043
* Duplicating field with select options results in two fields sharing same underlying options. #1735
* Fix drag and drop problem in Kanban view regarding inconsistent stack counts #1738
* Fix browser freeze in older browsers when copying single cell #1741
* Hide login buttons and login actions when afterSignupStepComponents are being displayed #1747
* Fix issue where cachalot doesn't invalidate correctly `database_multipleselect_*` m2m tables. #1772
* Made Airtable import compatible with signed files.
* Added datetime formats to be able to parse different datetimes (with momentjs strict mode enabled). #1648

[1.12.0]
* Update Baserow to 1.19.0
* [Full changelog](https://gitlab.com/bramw/baserow/-/releases/1.19.0)

[1.12.1]
* Update Baserow to 1.19.1
* [Full changelog](https://gitlab.com/bramw/baserow/-/releases/1.19.1)
* Fix JavaScript not loading in Safari
* period update bug
* web-frontend image launches correct version.

[1.13.0]
* Update Baserow to 1.20.0
* [Full changelog](https://gitlab.com/bramw/baserow/-/releases/1.20.0)
* email notifications
* workspace level audit log
* search for calendar view
* new shortcuts
* context menu improvements

[1.13.1]
* Update Baserow to 1.20.1
* [Full changelog](https://gitlab.com/baserow/baserow/-/releases/1.20.1)
* Remember the last used view, per table, per user #1273
* add `split_part` formula function #1940
* introduce `DATABASE_OPTIONS` json string env var for setting additional `database_options`. #1949

[1.13.2]
* Update Baserow to 1.20.2
* [Full changelog](https://gitlab.com/baserow/baserow/-/releases/1.20.2)
* Fixed that the dropdowns in the filters context are being cut off #1965
* Fix row ID being truncated in the grid view with 4 digit numbers in some browsers
* Respect the canKeyDown method when starting the multiple selection via keyboard shortcut

[1.14.0]
* Update base image to 4.2.0

[1.15.0]
* Update Baserow to 1.21.0
* [Full changelog](https://gitlab.com/baserow/baserow/-/releases/1.21.0)

[1.15.1]
* Update Baserow to 1.21.1
* [Full changelog](https://gitlab.com/baserow/baserow/-/releases/1.21.1)

[1.15.2]
* Update Baserot to 1.21.2
* [Full changelog](https://gitlab.com/baserow/baserow/-/releases/1.21.2)
* Fixed file import bug in the frontend.

[1.16.0]
* Update Baserow to 1.22.0
* [Full changelog](https://gitlab.com/baserow/baserow/-/releases/1.22.0)

[1.16.1]
* Update Baserow to 1.22.1
* [Full changelog](https://gitlab.com/baserow/baserow/-/releases/1.22.1)

[1.16.2]
* Update Baserow to 1.22.2
* [Full changelog](https://gitlab.com/baserow/baserow/-/releases/1.22.2)
* Bug fixes, and performance and stability improvements.

[1.17.0]
* Update Baserow to 1.22.3
* [Full changelog](https://gitlab.com/baserow/baserow/-/releases/1.22.3)
* Logo, description and screenshot changes
* Multidomain support
* Clean up UserLogEntry table entries #1792
* Add segmentControl UI component #1918
* Add support to reference duration fields in the formula language. #2190
* Add formats with days to the duration field. #2217
* Allow custom public share URLs #2292
* Use primary row value in email notifications instead of just row ids #2293
* Show billable label only to workspace admins #2295
* Added instructions on how to deploy Baserow to Railway. #2308
* Update Django to 4.1.X #761

[1.17.1]
* Use WSGI to handle the API endpoints

[1.18.0]
* Update Baserow to 1.23.0
* [Full changelog](https://gitlab.com/baserow/baserow/-/releases/1.23.0)

[1.18.1]
* Update Baserow to 1.23.1
* [Full changelog](https://gitlab.com/baserow/baserow/-/releases/1.23.1)
* Bug fixes and small changes

[1.18.2]
* Update Baserow to 1.23.2
* [Full changelog](https://gitlab.com/baserow/baserow/-/releases/1.23.2)
* Fix `KeyError` when using ASGI workers by making the `get_model` method thread safe.

[1.18.3]
* Fix nginx configuration

[1.19.0]
* Update Baserow to 1.24.0
* [Full changelog](https://gitlab.com/baserow/baserow/-/releases/1.24.0)

[1.19.1]
* Update Baserow to 1.24.1
* [Full changelog](https://gitlab.com/baserow/baserow/-/releases/1.24.1)
* Fixed role selecting bug

[1.19.2]
* Update Baserow to 1.24.2
* [Full changelog](https://gitlab.com/baserow/baserow/-/releases/1.24.2)

[1.20.0]
* Update Baserow to 1.25.0
* [Full changelog](https://gitlab.com/baserow/baserow/-/releases/1.25.0)

[1.20.1]
* Update Baserow to 1.25.1
* [Full changelog](https://gitlab.com/baserow/baserow/-/releases/1.25.1)

[1.20.2]
* Update Baserow to 1.25.2
* [Full changelog](https://gitlab.com/baserow/baserow/-/releases/1.25.2)

[1.21.0]
* Update Baserow to 1.26.0
* [Full changelog](https://gitlab.com/baserow/baserow/-/releases/1.26.0)

[1.21.1]
* Update Baserow to 1.26.1
* [Full changelog](https://gitlab.com/baserow/baserow/-/releases/1.26.1)

[1.22.0]
* Update Baserow to 1.27.1
* [Full changelog](https://gitlab.com/baserow/baserow/-/releases/1.27.1)

[1.22.1]
* Update Baserow to 1.27.2
* [Full changelog](https://gitlab.com/baserow/baserow/-/releases/1.27.2)

[1.23.0]
* Update Baserow to 1.28.0
* [Full changelog](https://gitlab.com/baserow/baserow/-/releases/1.28.0)
* Add the timeline view. #1262
* Allow to filter calendar view #1633
* Add data sync backend. #2278
* Introduced create data sync table frontend. #2278
* Introduced the ability to sync with a local Baserow table. #2278
* Optionally prevent sending row realtime update signals. #2278
* Introduced `read_only`, `immutable_type`, and `immutable_properties` state for the field. #2278
* Introduced the ability to manually trigger a sync. #2278
* [Builder] Add related record selector element #2396

[1.24.0]
* Update baserow to 1.29.0
* [Full Changelog](https://gitlab.com/baserow/baserow/-/releases/1.29.0)
* Use backend and frontend matching collate sort for single select, multiple select, and multiple collaborators field types. [#&#8203;1944](https://gitlab.com/baserow/baserow/-/issues/1944)
* \[Builder] Only return Data Source field data when used by a formula in a Page. [#&#8203;2063](https://gitlab.com/baserow/baserow/-/issues/2063)
* \[Builder] Allow sharing data sources across pages [#&#8203;2486](https://gitlab.com/baserow/baserow/-/issues/2486)
* \[Builder] Allow collection elements to be filtered, sorted and searched against. [#&#8203;2516](https://gitlab.com/baserow/baserow/-/issues/2516)
* Add Anthropic AI integration. [#&#8203;2564](https://gitlab.com/baserow/baserow/-/issues/2564)
* Allow to limit concurency for uvicorn workers in asgi backend. [#&#8203;2773](https://gitlab.com/baserow/baserow/-/issues/2773)
* Allow exporting workspace applications [#&#8203;2930](https://gitlab.com/baserow/baserow/-/issues/2930)
* Allow importing workspace applications [#&#8203;2951](https://gitlab.com/baserow/baserow/-/issues/2951)

[1.24.1]
* Update baserow to 1.29.2
* [Full Changelog](https://gitlab.com/baserow/baserow/-/releases/1.29.2)
* Boolean lookup filters: empty/not empty/none of/any/all [#&#8203;3103](https://gitlab.com/baserow/baserow/-/issues/3103)
* Copy cells as rich text table and optionally include header.

[1.24.2]
* Update baserow to 1.29.3
* [Full Changelog](https://gitlab.com/baserow/baserow/-/releases/1.29.3)
* Prevents webhook call tasks piling up if many webhooks are triggered. [#&#8203;2758](https://gitlab.com/baserow/baserow/-/issues/2758)
* Prevent triggering webhook if query param is provided on row create update or delete endpoints [#&#8203;3085](https://gitlab.com/baserow/baserow/-/issues/3085)
* limit logging sign in actions [#&#8203;3206](https://gitlab.com/baserow/baserow/-/issues/3206)
* Fix a bug where setting the User Role field to a Formula field could raise an error. [#&#8203;3190](https://gitlab.com/baserow/baserow/-/issues/3190)
* Fix for prefill values are lost for password protected forms [#&#8203;3194](https://gitlab.com/baserow/baserow/-/issues/3194)
* Fix form view conditions groups export import [#&#8203;3232](https://gitlab.com/baserow/baserow/-/issues/3232)
* Fix password protected shared view login page
* Remove login view vertical scrollbar on Safari iOS
* Redesigned auth views [#&#8203;1918](https://gitlab.com/baserow/baserow/-/issues/1918)

[1.25.0]
* Update baserow to 1.30.0
* [Full Changelog](https://gitlab.com/baserow/baserow/-/releases/1.30.0)
* Add support for advanced number formatting [#&#8203;1270](https://gitlab.com/baserow/baserow/-/issues/1270)
* Add ability to group by link to table field. [#&#8203;2306](https://gitlab.com/baserow/baserow/-/issues/2306)
* \[Builder] UX improvements: defaults for theme inputs, optional hash prefix in hex color inputs, and padding in Theme page. [#&#8203;3067](https://gitlab.com/baserow/baserow/-/issues/3067)
* Introduced ability to configure exising data sync. [#&#8203;3072](https://gitlab.com/baserow/baserow/-/issues/3072)
* Duration formula field filters [#&#8203;3110](https://gitlab.com/baserow/baserow/-/issues/3110)
* HubSpot contacts data sync. [#&#8203;3119](https://gitlab.com/baserow/baserow/-/issues/3119)
* Provide alias names for field types to search for in the dropdown list. [#&#8203;3179](https://gitlab.com/baserow/baserow/-/issues/3179)
* Export table/view to Excel file [#&#8203;324](https://gitlab.com/baserow/baserow/-/issues/324)
* Add support to filter multiple select formula fields. [#&#8203;3276](https://gitlab.com/baserow/baserow/-/issues/3276)
* Add ability to sort by link_row ('Link to table') field [#&#8203;506](https://gitlab.com/baserow/baserow/-/issues/506)
* Allow choosing select options in the form view. [#&#8203;814](https://gitlab.com/baserow/baserow/-/issues/814)
* Add sort and group by add field search.
* Added Korean language and translations

[1.25.1]
* Update baserow to 1.30.1
* [Full Changelog](https://gitlab.com/baserow/baserow/-/releases/1.30.1)
* Non-standard image formats don't break thumbnails [#&#8203;2745](https://gitlab.com/baserow/baserow/-/issues/2745)
* handle null payload for file field [#&#8203;2906](https://gitlab.com/baserow/baserow/-/issues/2906)
* Fix search for number field with applied formating [#&#8203;3292](https://gitlab.com/baserow/baserow/-/issues/3292)
* Fix a bug that prevent a database index to be created when sorting/grouping by a duration field. [#&#8203;3296](https://gitlab.com/baserow/baserow/-/issues/3296)
* Fix admin dashboard translations
* Fix loading non en and fr translations for premium and enterprise in production mode.
* Fix API docs server side rendering bugs.

[1.26.0]
* Update baserow to 1.31.0
* [Full Changelog](https://gitlab.com/baserow/baserow/-/releases/1.31.0)
* Allow optionally exporting in publicly shared views. [#&#8203;1213](https://gitlab.com/baserow/baserow/-/issues/1213)
* Allow skipping files during Airtable import [#&#8203;1225](https://gitlab.com/baserow/baserow/-/issues/1225)
* \[Builder] Introducing query parameters [#&#8203;1661](https://gitlab.com/baserow/baserow/-/issues/1661)
* \[Builder] Ensure that sensitive Integration data is not exported. [#&#8203;1863](https://gitlab.com/baserow/baserow/-/issues/1863)
* Add support for the Collaborator field type in formulas and lookups. [#&#8203;1890](https://gitlab.com/baserow/baserow/-/issues/1890)
* \[Builder] Enhance security by excluding unused fields from the Data Source context. [#&#8203;3001](https://gitlab.com/baserow/baserow/-/issues/3001)
* Add distribution aggregation. [#&#8203;3056](https://gitlab.com/baserow/baserow/-/issues/3056)
* \[Builder] Added radius settings for Element background and border. [#&#8203;3068](https://gitlab.com/baserow/baserow/-/issues/3068)
* \[Builder] Add support for Custom Colors in Theme settings. [#&#8203;3068](https://gitlab.com/baserow/baserow/-/issues/3068)
* \[Builder] Add vertical and horizontal gap to Repeat Element. [#&#8203;3068](https://gitlab.com/baserow/baserow/-/issues/3068)
* \[Builder] Added the ability to set the Font Weight for various elements in the Theme settings. [#&#8203;3068](https://gitlab.com/baserow/baserow/-/issues/3068)
* Periodic data sync [#&#8203;3071](https://gitlab.com/baserow/baserow/-/issues/3071)

[1.26.1]
* Update baserow to 1.31.1
* [Full Changelog](https://gitlab.com/baserow/baserow/-/releases/1.31.1)
* \[Builder] Resolved an issue in data source filters and sortings which prevented them from working correctly when the database field they were associated with were trashed. [#&#8203;3378](https://gitlab.com/baserow/baserow/-/issues/3378)
* Fixed an issue where the notification mode settings were not displayed correctly if the row was not in the view. [#&#8203;3411](https://gitlab.com/baserow/baserow/-/issues/3411)
* Disable optimistic updates for rows with filters, sorts, or group by on backend-calculated read-only fields. [#&#8203;3415](https://gitlab.com/baserow/baserow/-/issues/3415)
* Fixed issue where collaborator names were not displayed in filter when scrolling or searching [#&#8203;3431](https://gitlab.com/baserow/baserow/-/issues/3431)
* \[Builder] Resolved an issue which prevented tables from paging correctly.
* Fix indentation bug where duplicate table entries were added.
* Fix iCal feed when the table has select_related fields from enhance_queryset.

[1.27.0]
* checklist added to manifest

