#!/bin/bash

set -eu

mkdir -p /app/data/media

if [[ ! -f /app/data/.secret ]]; then
    echo "export SECRET_KEY=$(tr -dc 'a-z0-9' < /dev/urandom | head -c50)" > /app/data/.secret
fi
source /app/data/.secret

if [[ ! -f /app/data/env.sh ]]; then
    echo -e "# Add Baserow customizations here (https://baserow.io/docs/installation/configuration)\n\nexport BASEROW_BACKEND_LOG_LEVEL=INFO\n" > /app/data/env.sh
fi
source /app/data/env.sh

export BASEROW_PUBLIC_URL="${CLOUDRON_APP_ORIGIN}"
export PRIVATE_BACKEND_URL="http://localhost:8000"
export DATABASE_URL="postgresql://${CLOUDRON_POSTGRESQL_USERNAME}:${CLOUDRON_POSTGRESQL_PASSWORD}@${CLOUDRON_POSTGRESQL_HOST}:${CLOUDRON_POSTGRESQL_PORT}/${CLOUDRON_POSTGRESQL_DATABASE}"
export REDIS_URL="redis://:${CLOUDRON_REDIS_PASSWORD}@${CLOUDRON_REDIS_HOST}"
export MEDIA_ROOT="/app/data/media"

export EMAIL_SMTP="true"
export EMAIL_SMTP_USE_TLS=""
export FROM_EMAIL="${CLOUDRON_MAIL_FROM}"
export EMAIL_SMTP_HOST="${CLOUDRON_MAIL_SMTP_SERVER}"
export EMAIL_SMTP_PORT="${CLOUDRON_MAIL_SMTP_PORT}"
export EMAIL_SMTP_USER="${CLOUDRON_MAIL_SMTP_USERNAME}"
export EMAIL_SMTP_PASSWORD="${CLOUDRON_MAIL_SMTP_PASSWORD}"

export DJANGO_SETTINGS_MODULE='baserow.config.settings.base'
export MIGRATE_ON_STARTUP=false
export BASEROW_TRIGGER_SYNC_TEMPLATES_AFTER_MIGRATION=false

echo "==> Changing ownership"
chown -R cloudron:cloudron /app/data

echo "==> Executing database migrations"
gosu cloudron:cloudron /app/code/env/bin/python /app/code/backend/src/baserow/manage.py migrate
 
echo "==> Syncing templates (in the background)"
gosu cloudron:cloudron /app/code/env/bin/python /app/code/backend/src/baserow/manage.py sync_templates &

echo "==> Starting Baserow"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i Baserow

